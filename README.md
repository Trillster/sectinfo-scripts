# SECTINFO UDB Scripts

Heavily inspired by Thunderono's [SECTINFO tool](https://cutstuff.net/forum/index.php?topic=11955.0), this set of scripts can be used with [Ultimate Doom Builder](https://github.com/UltimateDoomBuilder/UltimateDoomBuilder) to be able to generate SECTINFO definition right inside of the map editor. 

Additionally, as a bonus, there's extra scripts for easily adding callouts to sectors or selecting all sectors with a given callout. Just like the aforementioned SECTINFO tool, Domination support is here too!

[Download Here!](https://gitlab.com/Trillster/sectinfo-scripts/-/archive/main/sectinfo-scripts-main.zip)

## Installation Guide

First, open any map in Ultimate Doom Builder. Once the map is open, click the `Scripts` tab on the right side of the screen.

![](/README/scripts.png)

This'll open a navigation pane where you can view and run all of the scripts you have. Inside of this navigation pane, right click on the `Examples` folder and click `Open in Explorer`.

![](/README/examples.png)

This'll take you to the directory that the `Examples` scripts are stored within. We'll want to navigate back one directory to the `Scripts` directory which is the root of Ultimate Doom Builder's script storage, so click the `Scripts` piece of the directory path.

![](/README/back.png)

Next, you'll need to download the SECTINFO scripts, so [click the download link here](https://gitlab.com/Trillster/sectinfo-scripts/-/archive/main/sectinfo-scripts-main.zip).

Once you have it downloaded, navigate inside of the ZIP file until you locate the `SECTINFO` folder. 

![](/README/zip.png)

Drag the `SECTINFO` folder into Ultimate Doom Builder's `Scripts` directory.

![](/README/drag.png)

If successfully done, you'll see a brand new set of scripts in Ultimate Doom Builder. Enjoy!

![](/README/done.png)