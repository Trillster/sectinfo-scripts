/// <reference path="../udbscript.d.ts" />

`#version 4`;

`#name Change Sector Callout`;

`#description Add a callout to selected sectors. Sets 'user_callout' sector field to denote callout.`;

const flavor = 'callout';
const type = 'user_callout';
const userField = 'Use Existing Callout';
const callout = 'Create New Callout';
const calloutDefault = 'New Callout Name';

const selectedSectors = UDB.Map.getSelectedOrHighlightedSectors();
if(selectedSectors.length <= 0) {
    UDB.showMessage("No sectors selected or highlighted.");
    UDB.die("No sectors selected or highlighted.");
}

const sectors = UDB.Map.getSectors();
const callouts = {};
for(let sector of sectors) {
    const name = sector.fields[type];
    if(name) {
        if(name in callouts) {
            callouts[name].push(sector);
        } else {
            callouts[name] = [ sector ];
        }
    }
}

const TYPE_STRING = 2;
const TYPE_ENUM = 11;

const sortedCallouts = Object.keys(callouts).sort();
const queryEnum = { "-1": "[Create New]" };
let i = 0;
for(let callout of sortedCallouts) {
    queryEnum[i] = callout;
    i++;
}

const query = new UDB.QueryOptions();
query.addOption('userField', userField, TYPE_ENUM, -1, queryEnum);
query.addOption('callout', callout, TYPE_STRING, calloutDefault);
if(!query.query()) {
    UDB.exit('Aborted script.');
}

const selectedCallout = query.options.userField === -1 ? query.options.callout : queryEnum[query.options.userField];
for(let sector of selectedSectors) {
    sector.fields[type] = selectedCallout;
}
UDB.exit(`Updated ${selectedSectors.length} sectors to use ${flavor} '${selectedCallout}'`);
