/// <reference path="../udbscript.d.ts" />

`#version 4`;

`#name Select Point Sectors`;

`#description Select all sectors with a given point name. Uses 'user_point' sector field to denote point name.`;

const flavor = 'point name';
const type = 'user_point';
const userField = 'Point Name';

const sectors = UDB.Map.getSectors();
const callouts = {};
const noCalloutSectors = [];
for(let sector of sectors) {
    const name = sector.fields[type];
    if(name) {
        if(name in callouts) {
            callouts[name].push(sector);
        } else {
            callouts[name] = [ sector ];
        }
    } else {
        noCalloutSectors.push(sector);
    }
}

const TYPE_ENUM = 11;

const sortedCallouts = Object.keys(callouts).sort();
const queryEnum = { '-1': `[No ${userField}]`};
let i = 0;
for(let callout of sortedCallouts) {
    queryEnum[i] = callout;
    i++;
}

const query = new UDB.QueryOptions();
query.addOption('userField', userField, TYPE_ENUM, -1, queryEnum);
if(!query.query()) {
    UDB.exit('Aborted script.');
}

UDB.Map.clearAllSelected();
if(query.options.userField === -1) {
    for(let sector of noCalloutSectors) {
        sector.selected = true;
    }
    UDB.exit(`Selected ${noCalloutSectors.length} sectors with no ${flavor}.`)
} else {
    const calloutSectors = callouts[queryEnum[query.options.userField]];
    for(let sector of calloutSectors) {
        sector.selected = true;
    }
    UDB.exit(`Selected ${calloutSectors.length} sectors with ${flavor} '${queryEnum[query.options.userField]}'.`)
}
