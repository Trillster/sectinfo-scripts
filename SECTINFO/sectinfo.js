/// <reference path="udbscript.d.ts" />

`#version 4`;

`#name Generate SECTINFO`;

`#description Generate SECTINFO for current map. Callouts are denoted by 'user_callout' sector fields and points are denoted by 'user_point' sector fields.`;

const TYPE_STRING = 2;

const query = new UDB.QueryOptions();
query.addOption('mapLump', 'Map lump name', TYPE_STRING, "MAPLUMP");
if(!query.query()) {
    UDB.exit('Aborted script.');
}

const mapLump = query.options.mapLump;
const sectors = UDB.Map.getSectors();
const callouts = {};
const points = {};

for(let sector of sectors) {
    const name = sector.fields.user_callout;
    if(name) {
        if(name in callouts) {
            callouts[name].push(sector.index);
        } else {
            callouts[name] = [ sector.index ];
        }
    }

    const point = sector.fields.user_point;
    if(point) {
        if(point in points) {
            points[point].push(sector.index);
        } else {
            points[point] = [ sector.index ];
        }
    }
}

let message = `[${mapLump}]\n`;
let i;

let max = Object.keys(callouts).length;
if(max > 0) {
    message += "names = {\n";

    let j = 0;
    for(let callout in callouts) {
        message += `\t"${callout}" = {`;
        for(let i = 0; i < callouts[callout].length; i++) {
            message += callouts[callout][i] + (i + 1 === callouts[callout].length ? '' : ', ');
        }
        j++;
        message += `}${j === max ? '' : ','}\n`;
    }

    message += "}\n";
}

max = Object.keys(points).length;
if(max > 0) {
    message += "points = {\n";

    let j = 0;
    for(let point in points) {
        message += `\t"${point}" = {`;
        for(let i = 0; i < points[point].length; i++) {
            message += points[point][i] + (i + 1 === points[point].length ? '' : ', ');
        }
        j++;
        message += `}${j === max ? '' : ','}\n`;
    }
    message += "}\n";
}

UDB.showMessage(message);
UDB.exit(`Successfully generated SECTINFO.`)
